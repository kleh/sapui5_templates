sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/core/routing/History"
	], function (Controller, History) {
	"use strict";

	return Controller.extend("kl.app.sapui5.controller.BaseController", {
		getResourceBundle : function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Navigates back in the browser history, if the entry was created by this app.
		 * If not, it navigates to a route passed to this function.
		 */
		myNavBack : function(sRoute, mData, router) {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				// The history contains a previous entry

				window.history.go(-1);

			} else {
				// Otherwise we go backwards with a forward history
				var bReplace = true;
				router.navTo(sRoute, mData, bReplace);
			}
		}
	});
});
