sap.ui.define([
		"kl/app/sapui5/controller/BaseController"
	], function (BaseController) {
	"use strict";

	return BaseController.extend("kl.app.sapui5.controller.Detail", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
            // nothing to do at the moment
		},

    _onObjectMatched : function (oEvent) {
			var sPar =  oEvent.getParameter("arguments").ID;
			console.log(sPar);
		},

    onNavBack : function () {
			this.myNavBack("master");
		},



		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */


	});

});
