sap.ui.define([
		"kl/app/sapui5/controller/BaseController"
	], function (BaseController) {
	"use strict";

	return BaseController.extend("kl.app.sapui5.controller.Detail", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
			this.router = sap.ui.core.UIComponent.getRouterFor(this);
			this.router.attachRoutePatternMatched(this.onRouteMatched, this);
		},

    onRouteMatched : function (oEvent) {
			var sPar =  oEvent.getParameter("arguments").ID;
			console.log(sPar);
		},

    onNavBack : function () {
			this.myNavBack("master",{}, this.router);
		},



		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */


	});

});
