sap.ui.define([
		"sap/ui/core/UIComponent",
		"sap/ui/model/resource/ResourceModel",
		"sap/ui/model/json/JSONModel"
	], function (UIComponent, ResourceModel, JSONModel) {
	"use strict";

	return UIComponent.extend("kl.app.sapui5.Component", {

		metadata : {
			routing : {
				"config": {
					viewType : "XML",
					viewPath : "kl.app.sapui5.view",
					targetControl : "app",
					clearTarget : false,
					transition: "slide"

				},
				routes : [

					{
						pattern : "",
						name : "main",
						viewPath : "kl.app.sapui5.view",
						view : "Master",
						viewLevel : 0,
						viewType: "XML",
						targetAggregation : "pages"

					},
					{
						pattern : "detail/:ID:",
						name : "detail",
						view : "Detail",
						viewPath : "kl.app.sapui5.view",
						viewLevel : 1,
						viewType: "XML",
						targetAggregation : "pages"
					}

				]

			}
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this function, the resource and application models are set and the router is initialized.
		 * @public
		 * @override
		 */
		init : function () {
			// 1. some very generic requires
			jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
			//jQuery.sap.require("webapp.MyRouter");


			// 2. call overridden init (calls createContent)
			sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

			// 3a. monkey patch the router
			var router = this.getRouter();

			// 4. initialize the router
			this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
			router.initialize();
		},
		destroy : function () {
			if (this.routeHandler) {
				this.routeHandler.destroy();
			}

			// call overridden destroy
			sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
		},

		createContent : function () {
			// create root view
			var oView = sap.ui.view({
				id : "app",
				viewName : "kl.app.sapui5.view.App",
				type : "XML",
				viewData : { component : this }
			});

			// Set model to service

			//var oModel = new sap.ui.model.odata.ODataModel("/orderapp/services/orders.xsodata"
			//		,{tokenHandling:true});
			//oModel.setDefaultBindingMode("TwoWay");
			//oView.setModel(oModel);

			var i18nModel = new ResourceModel({
							bundleName: "kl.app.sapui5.i18n.i18n"
					 });
					oView.setModel(i18nModel, "i18n");


			// set device model
			var oDeviceModel = new JSONModel({
				isTouch : sap.ui.Device.support.touch,
				isNoTouch : !sap.ui.Device.support.touch,
				isPhone : sap.ui.Device.system.phone,
				isNoPhone : !sap.ui.Device.system.phone,
				listMode : sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
				listItemType : sap.ui.Device.system.phone ? "Active" : "Inactive"
			});
			oDeviceModel.setDefaultBindingMode("OneWay");
			oView.setModel(oDeviceModel, "device");

			// done
			return oView;
		}
	});
});
