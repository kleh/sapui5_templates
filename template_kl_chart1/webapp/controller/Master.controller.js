sap.ui.define([
		"kl/app/sapui5/controller/BaseController"
	], function (BaseController) {
	"use strict";

	return BaseController.extend("kl.app.sapui5.controller.Master", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function() {
			var cntr = this;
			this.getView().addEventDelegate({
				"onAfterRendering":function(){
						cntr.createChart();
					}
				}, this);

			this.sd = [1,3,5,2,4,6,3,9,8,5,1,3,3,2,4,6,3,1,2,3,2,2,2,2,2,11,21,3,4,4,4,4,4];
		},


		onButton: function(oEvt){
			var d = this.myLiveChart.data.datasets[0].data;
			this.myLiveChart.data.datasets[0].data.push(this.sd.shift());
			this.myLiveChart.data.datasets[0].data.shift();
			this.myLiveChart.update();
		},

		createChart: function(){
				Chart.defaults.global.animation.duration=2;

				var ctx = document.getElementById('__xmlview1--chartarea');
    		this.myLiveChart = new Chart(ctx, {
					type: 'line',

		      data: {
						labels: [1, 2, 3, 4, 5, 6, 7],
						datasets: [
			          {
			              data: [5, 9, 8, 1, 6, 5, 4]
			          }
			      	]
						},
					options: {
	        	scales: {
								yAxes: [
									{
										ticks: {
										max: 26,
										min: 0,
										stepSize: 2
										}
									}
								]
							}
					}
		    });

		},


		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */


	});

});
