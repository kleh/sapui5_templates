sap.ui.define([
		"kl/app/sapui5/controller/BaseController"
	], function (BaseController) {
	"use strict";

	return BaseController.extend("kl.app.sapui5.controller.Master", {

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
            // nothing to do at the moment
		},



		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

	
	});

});
